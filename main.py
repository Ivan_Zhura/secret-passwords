import re
from os import getcwd, remove
from os.path import isfile, join
from random import choice, shuffle

from PyQt6.QtWidgets import QApplication
import database
import encryption
from gui import (
    QtCore,
    QPixmap,
    MainWindowGUI,
    PasswordsWindowGUI,
    EntryWindowGUI
)
from warning_messages import (
    NOT_UNIQUE_PASSWORD_WARNING,
    NOT_FILLED_PASSWORD_WARNING,
    WRONG_SYMBOLS_IN_PASSWORD_WARNING,
    EMPTY_LOGIN_WARNING,
    NOT_UNIQUE_LOGIN_WARNING
)

# TODO: сделать функцию кэширования всех логинов и паролей в текущей сессии работы в словарь стр. 81


KEY = None

ENTRY_WINDOW = None
MAIN_WINDOW = None
PASSWORDS_WINDOW = None
CLIPBOARD = None


class EntryWindow(EntryWindowGUI):
    """Login window logic"""
    def __init__(self):
        super().__init__()
        self.attempt_number = 10
        self.attempt.setProperty('value', self.attempt_number)
        self.entry_button.clicked.connect(self.verify_key)
        self.create_button.clicked.connect(self.verify_identical_keys)
        self.verify_entry.returnPressed.connect(self.verify_identical_keys)
        if isfile(join(getcwd(), database.ENCRYPTED_DB_NAME)):
            self.display_login_window()
            self.key_entry.returnPressed.connect(self.verify_key)
        else:
            self.key_entry.returnPressed.connect(self.focus_on_verify_entry)
            self.display_key_create_window()
        self.v_box.addStretch()
        # set main window layout:
        self.setLayout(self.v_box)

    def verify_identical_keys(self):
        global KEY, DB
        KEY = self.key_entry.text()
        key_verify = self.verify_entry.text()
        if KEY == key_verify:
            DB = database.DataBase()
            DB.db_create()
            self.show_main_window()
        else:
            self.verify_entry.setStyleSheet('background-color: rgb(253, 187, 188)')
            self.warning_label.setText('Пароли не совпадают!')

    def verify_key(self):
        """
        This method invites you to create a password which will be used to
        encryption of database and checks if the correct password is being used
        """
        global KEY, DB
        KEY = self.key_entry.text()
        try:
            encryption.decrypt_database(database.DB_NAME, KEY)
            self.show_main_window()
            DB = database.DataBase()
        except ValueError:  # password incorrect
            self.warning_label.setText('Неверный пароль!')
            self.key_entry.setStyleSheet('background-color: rgb(253, 187, 188)')
            self.warning_attempt.setText(f'Осталось попыток: {self.attempt_number}')
            self.attempt.setValue(self.attempt_number)
            self.attempt.show()
            self.pix = QPixmap('src/images/lock_wrong.png')
            self.image_label.setPixmap(self.pix)
            # if app password is entered incorrectly 10 times the
            # database with passwords is deleted
            if self.attempt_number > 0:
                if self.attempt_number == 1:
                    pass
                self.attempt_number -= 1
            else:
                self.warning_label.setText('База данных удалена!')
                try:
                    remove(database.ENCRYPTED_DB_NAME)
                except FileNotFoundError:
                    self.close()

    def focus_on_verify_entry(self):
        if isfile(join(getcwd(), database.DB_NAME)):
            self.verify_key()
        else:
            self.verify_entry.setFocus()

    def show_main_window(self):
        global MAIN_WINDOW
        MAIN_WINDOW = MainWindow()
        MAIN_WINDOW.show()
        self.close()


class MainWindow(MainWindowGUI):
    """Logic for window for passwords generation"""
    def __init__(self):
        super().__init__()
        self.generate_button.clicked.connect(self.insert_generated_password)
        self.copy_button.clicked.connect(self.copy_password)
        self.save_button.clicked.connect(self.save_password)
        self.copy_button.clicked.connect(self.copy_password)
        self.show_button.clicked.connect(self.show_passwords)
        self.login_entry.returnPressed.connect(self.insert_generated_password)
        self.login_entry.clicked.connect(self.clear_login_warning)
        self.password_entry.clicked.connect(self.clear_password_warning)

    @staticmethod
    def generate_password():
        """Method for creating strong random passwords"""
        numbers = '0123456789'
        small_letters = 'qwertyuiopasdfghjklzxcvbnm'
        capital_letters = small_letters.swapcase()
        special_symbols = '~<>$#%*!?@&+-'

        l1 = 8  # number of digits in your password
        l2 = 10  # number of lower case in your password
        l3 = 10  # number of upper case in your password
        l4 = 4  # number of special characters in your password

        password_list = []

        for i in range(l1):
            password_list.append(choice(numbers))
        for i in range(l2):
            password_list.append(choice(small_letters))
        for i in range(l3):
            password_list.append(choice(capital_letters))
        for i in range(l4):
            password_list.append(choice(special_symbols))

        shuffle(password_list)
        password = ''.join(password_list)
        return password

    def validate_login(self, login):
        """Method for validating input login data"""
        if login == '':
            self.notify_wrong_login(EMPTY_LOGIN_WARNING)
            return False
        if len(DB.db_read(login).fetchall()):
            self.notify_wrong_login(NOT_UNIQUE_LOGIN_WARNING)
            return False
        return True

    def validate_password(self, password):
        """Method for validating input password data"""
        global previous_password
        if re.search(r'[.А-яа-я]+', password) is not None:
            self.notify_wrong_password(WRONG_SYMBOLS_IN_PASSWORD_WARNING)
            return False
        if password == '':
            self.notify_wrong_password(NOT_FILLED_PASSWORD_WARNING)
            return False
        if password == previous_password:
            self.notify_wrong_password(NOT_UNIQUE_PASSWORD_WARNING)
            return False
        return True

    def insert_generated_password(self):
        self.restore_original_window_view()
        self.password_entry.clear()
        login = self.login_entry.text()
        if self.validate_login(login):
            password = self.generate_password()
            self.notify_password_generate_success()
            self.password_entry.insert(password)

    def save_password(self):
        global previous_password
        self.restore_original_window_view()
        login = self.login_entry.text()
        password = self.password_entry.text()
        login_is_valid = self.validate_login(login)
        password_is_valid = self.validate_password(password)
        if login_is_valid and password_is_valid:
            previous_password = password
            DB.db_write(login, password)
            self.notify_password_saved_success()

    def copy_password(self):
        if self.password_entry.text() != '':
            CLIPBOARD.setText(self.password_entry.text())

    @staticmethod
    def show_passwords():
        global PASSWORDS_WINDOW
        PASSWORDS_WINDOW = PasswordsWindow()
        PASSWORDS_WINDOW.show()

    def closeEvent(self, event):
        encryption.encrypt_database(database.DB_NAME, KEY)


class PasswordsWindow(PasswordsWindowGUI):
    """Window for viewing passwords"""
    def __init__(self):
        super().__init__()
        self.search_button.clicked.connect(self.search_login)
        self.show_all_button.clicked.connect(self.show_all_passwords)
        self.search_entry.clicked.connect(self.restore_search_results)
        self.search_entry.returnPressed.connect(self.search_login)
        self.result = dict(DB.db_read_all())
        self.show_all_passwords()

    def show_all_passwords(self):
        self.clear_all_search_results()
        for index, (login, password) in enumerate(self.result.items()):
            self.show_login_and_password(login, password, index)

    def search_login(self):
        self.clear_all_search_results()
        login = self.search_entry.text()
        search_result = dict(DB.db_search(login))
        if not search_result:
            self.notify_login_does_not_exists(login)
        else:
            self.search_entry.setStyleSheet('background-color: white;')
            for index, (login, password) in enumerate(search_result.items()):
                self.show_login_and_password(login, password, index)
            self.results_widget.show()

    def restore_search_results(self):
        self.clear_search_entry()
        self.show_all_passwords()


if __name__ == "__main__":
    import sys
    previous_password = ''
    app = QApplication(sys.argv)
    CLIPBOARD = app.clipboard()
    ENTRY_WINDOW = EntryWindow()
    ENTRY_WINDOW.show()
    sys.exit(app.exec())
