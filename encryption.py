import os
import pyAesCrypt


KEY = None
BUFFER_SIZE = 512 * 1024


def encrypt_database(db_file, password):
    try:
        pyAesCrypt.encryptFile(
            infile=db_file,
            outfile="_" + db_file,
            passw=password,
            bufferSize=BUFFER_SIZE
        )
        os.remove(db_file)
    except ValueError:
        pass


def decrypt_database(db_file, password):
    pyAesCrypt.decryptFile(
        infile="_" + db_file,
        outfile=db_file,
        passw=password,
        bufferSize=BUFFER_SIZE
    )
    os.remove("_" + db_file)
