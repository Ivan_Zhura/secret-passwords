import sqlite3


DB_NAME = 'mypasswords.db'
ENCRYPTED_DB_NAME = '_mypasswords.db'


class DataBase:
    """Class to work with sqlite database"""

    def __init__(self):
        self.conn = sqlite3.connect(DB_NAME)
        self.cursor = self.conn.cursor()

    def db_create(self):
        """Create database table 'pass'"""
        self.cursor.execute('''CREATE TABLE pass (
                           login TEXT NOT NULL PRIMARY KEY UNIQUE ,
                           password TEXT NOT NULL)''')
        self.conn.commit()

    def db_write(self, login_value, password):
        """Write password and login to the database."""
        self.cursor.execute('''INSERT INTO pass (login, password)
        VALUES (?, ?)''', (login_value, password))
        self.conn.commit()

    def db_update(self, login_value, password):
        """Update login or password in the database."""
        self.cursor.execute('UPDATE pass SET password=? WHERE login=?', (password, login_value))
        self.conn.commit()

    def db_read_all(self):
        """Connect to the database and returns table with your passwords"""
        return self.cursor.execute("SELECT * FROM pass")

    def db_read(self, login):
        """
        Connect to the database and returns
        password and login for the given login
        """
        return self.cursor.execute(f"SELECT * FROM pass WHERE login='{login}'")

    def db_search(self, login):
        """Search login in database"""
        return self.cursor.execute(f"SELECT * FROM pass WHERE login LIKE '%{login}%'")
