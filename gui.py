import re

from PyQt6 import QtCore
from PyQt6.QtGui import (QFont, QPixmap, QIcon)
from PyQt6.QtWidgets import (
    QWidget,
    QVBoxLayout,
    QHBoxLayout,
    QGridLayout,
    QPushButton,
    QLabel,
    QLineEdit,
    QSizePolicy,
    QProgressBar,
    QScrollArea
)
from warning_messages import (
    NOT_UNIQUE_PASSWORD_WARNING,
    NOT_FILLED_PASSWORD_WARNING,
    WRONG_SYMBOLS_IN_PASSWORD_WARNING,
    EMPTY_LOGIN_WARNING,
    NOT_UNIQUE_LOGIN_WARNING
)

with open('style.css') as style_file:
    STYLE = style_file.read()


class ClickableQLineEdit(QLineEdit):
    """QLineEdit with clicked signal"""
    clicked = QtCore.pyqtSignal()

    def __init__(self, widget):
        super().__init__(widget)

    def mousePressEvent(self, QMouseEvent):
        self.clicked.emit()


class BaseWindowsSettings(QWidget):
    def __init__(self):
        super().__init__()
        self.resize(560, 420)
        self.setWindowTitle('Secure Passwords')
        # font settings:
        self.font = QFont()
        self.font.setFamily('SansSerif')
        self.font.setPointSize(12)
        # init main QVBoxLayout:
        self.v_box = QVBoxLayout()
        self.setStyleSheet(STYLE)


class EntryWindowGUI(BaseWindowsSettings):
    """Entry window of the app"""
    def __init__(self):
        super().__init__()
        # lock image settings:
        self.image_label = QLabel()
        self.pix = QPixmap('src/images/lock_welcome.png')
        self.image_label.setPixmap(self.pix)
        self.image_label.setSizePolicy(
            QSizePolicy(QSizePolicy.Policy.Fixed, QSizePolicy.Policy.Fixed)
        )
        self.image_label.setMinimumSize(QtCore.QSize(150, 180))
        self.image_label.setMaximumSize(QtCore.QSize(150, 180))
        self.image_label.setScaledContents(True)
        # password entries layout:
        self.password_h_box = QHBoxLayout()
        self.password_h_box.setAlignment(QtCore.Qt.AlignmentFlag.AlignHCenter)
        # entry password settings:
        self.key_entry = QLineEdit()
        self.key_entry.setEchoMode(QLineEdit.EchoMode.Password)
        self.key_entry.setMinimumSize(QtCore.QSize(160, 32))
        self.key_entry.setMaximumSize(QtCore.QSize(320, 32))
        # labels:
        self.entry_label = QLabel('Добро пожаловать!')
        self.entry_label.setFont(self.font)
        self.entry_label.setObjectName('main_label')
        self.password_label = QLabel('Введите пароль')
        self.password_label.setObjectName('auxiliary_label')
        self.verify_label = QLabel('Повторите пароль')
        self.verify_label.setFont(self.font)
        self.verify_label.setObjectName('auxiliary_label')
        self.warning_label = QLabel()
        self.warning_label.setStyleSheet('color: red;')
        # verify password settings:
        self.verify_entry = QLineEdit()
        self.verify_entry.setEchoMode(QLineEdit.EchoMode.Password)
        self.verify_entry.setMinimumSize(QtCore.QSize(160, 32))
        self.verify_entry.setMaximumSize(QtCore.QSize(320, 32))
        # buttons:
        self.entry_button = QPushButton()
        self.entry_button.setIcon(QIcon('src/images/ok.png'))
        self.entry_button.setFont(self.font)
        self.entry_button.setFixedSize(32, 32)
        self.entry_button.setIconSize(QtCore.QSize(32, 32))
        self.entry_button.setObjectName('ok_button')
        self.create_button = QPushButton('Поехали!')
        self.create_button.setFixedSize(180, 32)
        self.create_button.setObjectName('text_button')
        # progress bar with attempts:
        self.warning_v_layout = QVBoxLayout()
        self.warning_h_layout = QHBoxLayout()
        self.warning_attempt = QLabel()
        self.warning_attempt.setStyleSheet('color: red;')
        self.attempt = QProgressBar()
        self.attempt.setMinimumSize(QtCore.QSize(50, 8))
        self.attempt.setMaximumSize(QtCore.QSize(160, 8))
        self.attempt.setMaximum(10)
        self.attempt.setInvertedAppearance(True)
        # building a window:
        self.v_box.addStretch()
        self.v_box.addWidget(
            self.image_label, 0, QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        # self.v_box.addStretch()
        self.password_h_box.addWidget(self.key_entry)
        self.password_h_box.addWidget(self.entry_button)
        self.warning_h_layout.addWidget(
            self.warning_attempt, 0, QtCore.Qt.AlignmentFlag.AlignRight
        )
        self.warning_v_layout.addWidget(
            self.warning_label, 0, QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.warning_h_layout.addWidget(
            self.attempt, 0, QtCore.Qt.AlignmentFlag.AlignLeft
        )
        self.attempt.hide()
        self.v_box.addStretch()

    def display_login_window(self):
        self.v_box.addWidget(
            self.entry_label, 0, QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.v_box.addLayout(self.password_h_box)
        self.v_box.addWidget(
            self.password_label, 0, QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.warning_v_layout.addLayout(self.warning_h_layout)
        self.v_box.addStretch()
        self.v_box.addLayout(self.warning_v_layout)

    def display_key_create_window(self):
        self.v_box.addWidget(
            self.entry_label, 0, QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.v_box.addWidget(
            self.key_entry, 0, QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.password_label.setText('Придумайте пароль')
        self.v_box.addWidget(
            self.password_label, 0, QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.v_box.addStretch()
        self.v_box.addWidget(
            self.verify_entry, 0, QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.v_box.addWidget(
            self.verify_label, 0, QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.v_box.addStretch()
        self.v_box.addWidget(
            self.create_button, 0, QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.v_box.addStretch()
        self.v_box.addLayout(self.warning_v_layout)
        self.repaint()


class MainWindowGUI(BaseWindowsSettings):
    """Main window of the app"""
    def __init__(self):
        super().__init__()
        # lock image:
        self.image_label = QLabel(self)
        self.pix = QPixmap('src/images/lock.png')
        self.image_label.setPixmap(self.pix)
        self.image_label.setSizePolicy(
            QSizePolicy(QSizePolicy.Policy.Fixed, QSizePolicy.Policy.Fixed)
        )
        self.image_label.setMinimumSize(QtCore.QSize(150, 180))
        self.image_label.setMaximumSize(QtCore.QSize(150, 180))
        self.image_label.setScaledContents(True)
        # input fields and labels layout:
        self.grid = QGridLayout()
        self.grid.setSpacing(8)
        # labels:
        self.login_label = QLabel('Введите логин: ')
        self.password_label = QLabel('Ваш пароль: ')
        self.login_label.setObjectName('auxiliary_label')
        self.password_label.setObjectName('auxiliary_label')
        # input fields:
        self.login_entry = ClickableQLineEdit(self)
        self.login_entry.setMinimumSize(QtCore.QSize(296, 32))
        self.password_entry = ClickableQLineEdit(self)
        self.password_entry.setMinimumSize(QtCore.QSize(296, 32))
        # buttons:
        self.buttons_layout = QGridLayout()
        self.buttons_layout.setSpacing(15)
        self.save_button = QPushButton('Сохранить пароль')
        self.save_button.setMinimumSize(180, 32)
        self.save_button.setObjectName('text_button')
        self.show_button = QPushButton('Просмотреть все пароли')
        self.show_button.setMinimumSize(200, 32)
        self.show_button.setObjectName('text_button')

        self.generate_button = QPushButton()
        self.generate_button.setIcon(QIcon('src/images/ok.png'))
        self.generate_button.setFont(self.font)
        self.generate_button.setFixedSize(32, 32)
        self.generate_button.setIconSize(QtCore.QSize(32, 32))
        self.generate_button.setObjectName('ok_button')

        self.copy_button = QPushButton()
        self.copy_button.setIcon(QIcon('src/images/copy.png'))
        self.copy_button.setFont(self.font)
        self.copy_button.setFixedSize(32, 32)
        self.copy_button.setIconSize(QtCore.QSize(32, 32))
        self.copy_button.setObjectName('ok_button')
        # success_label:
        self.success_label = QLabel()
        self.success_label.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        # building a window:
        # self.v_box.addStretch()
        self.v_box.addWidget(
            self.image_label, 0, QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.v_box.addStretch()
        self.grid.addWidget(self.login_label, 0, 0)
        self.grid.addWidget(self.login_entry, 0, 1)
        self.grid.addWidget(self.generate_button, 0, 2)
        self.grid.addWidget(self.password_label, 1, 0)
        self.grid.addWidget(self.password_entry, 1, 1)
        self.grid.addWidget(self.copy_button, 1, 2)
        self.v_box.addLayout(self.grid)
        self.v_box.addStretch()
        self.buttons_layout.addWidget(
            self.save_button, 0, 0, QtCore.Qt.AlignmentFlag.AlignLeft
        )
        self.buttons_layout.addWidget(
            self.show_button, 0, 1, QtCore.Qt.AlignmentFlag.AlignRight
        )
        self.v_box.addLayout(self.buttons_layout)
        self.v_box.addStretch()
        self.v_box.addWidget(
            self.success_label, 0, QtCore.Qt.AlignmentFlag.AlignVCenter
        )
        self.v_box.addStretch()
        self.setLayout(self.v_box)

    def restore_original_window_view(self):
        self.login_entry.setStyleSheet('background - color: rgb(255, 255, 255)')
        self.password_entry.setStyleSheet('background - color: rgb(255, 255, 255)')
        self.pix = QPixmap('src/images/lock.png')
        self.image_label.setPixmap(self.pix)
        self.success_label.clear()
        self.clear_login_warning()
        self.clear_password_warning()

    def notify_password_saved_success(self):
        self.login_entry.setStyleSheet('background - color: rgb(255, 255, 255)')
        self.success_label.setStyleSheet('color: rgb(68, 222, 44);')
        self.success_label.setText('Пароль успешно сохранен!')

    def notify_password_generate_success(self):
        self.login_entry.setStyleSheet('background - color: rgb(255, 255, 255)')
        self.success_label.setStyleSheet('color: rgb(68, 222, 44);')
        self.success_label.setText('Пароль успешно сгенерирован!')
        self.pix = QPixmap('src/images/lock.png')
        self.image_label.setPixmap(self.pix)

    def notify_wrong_password(self, text):
        self.success_label.clear()
        self.password_entry.setStyleSheet('background-color: rgb(253, 187, 188)')
        self.password_entry.clear()
        self.password_entry.setText(text)
        self.pix = QPixmap('src/images/lock_wrong.png')
        self.image_label.setPixmap(self.pix)

    def notify_wrong_login(self, text):
        self.success_label.clear()
        self.login_entry.setStyleSheet('background-color: rgb(253, 187, 188)')
        self.login_entry.clear()
        self.login_entry.setText(text)
        self.pix = QPixmap('src/images/lock_wrong.png')
        self.image_label.setPixmap(self.pix)

    def clear_password_warning(self):
        password = self.password_entry.text()
        if password in (NOT_FILLED_PASSWORD_WARNING,
                        NOT_UNIQUE_PASSWORD_WARNING,
                        WRONG_SYMBOLS_IN_PASSWORD_WARNING):
            self.password_entry.clear()
            self.password_entry.setStyleSheet('background - color: rgb(255, 255, 255)')

    def clear_login_warning(self):
        login = self.login_entry.text()
        if login in (NOT_UNIQUE_LOGIN_WARNING, EMPTY_LOGIN_WARNING):
            self.login_entry.clear()
            self.login_entry.setStyleSheet('background - color: rgb(255, 255, 255)')


class PasswordsWindowGUI(BaseWindowsSettings):
    def __init__(self):
        super().__init__()
        # search layout:
        self.search_layout = QGridLayout()
        self.search_layout.setAlignment(
            QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.search_layout.setSpacing(5)
        # search widget:
        self.search_widget = QWidget()
        self.search_widget.setLayout(self.search_layout)
        # show label:
        self.search_label = QLabel('Поиск записи:')
        self.search_label.setFont(self.font)
        self.search_label.setObjectName('auxiliary_label')
        # search entry:
        self.search_entry = ClickableQLineEdit(self)
        self.search_entry.setMinimumSize(QtCore.QSize(330, 32))
        # buttons:
        self.buttons_layout = QHBoxLayout()
        self.search_button = QPushButton('Найти запись')
        self.search_button.setMinimumSize(180, 32)
        self.show_all_button = QPushButton('Показать все записи')
        self.show_all_button.setMinimumSize(180, 32)
        self.search_button.setObjectName('text_button')
        self.show_all_button.setObjectName('text_button')
        # results area:
        self.results_labels_layout = QGridLayout()
        self.results_labels_layout.setAlignment(
            QtCore.Qt.AlignmentFlag.AlignTop
        )
        self.login_label = QLabel('Логин')
        self.password_label = QLabel('Пароль')
        self.login_label.setObjectName('auxiliary_label')
        self.password_label.setObjectName('auxiliary_label')

        self.results_area = QScrollArea()
        self.results_area.sizePolicy().setVerticalStretch(0)
        self.results_area.setWidgetResizable(True)
        self.results_area.setMinimumSize(520, 300)
        self.results_main_widget = QWidget()
        self.results_v_layout = QVBoxLayout()
        self.results_v_layout.setAlignment(QtCore.Qt.AlignmentFlag.AlignTop)

        self.results_widget = QWidget()
        self.search_result_layout = QGridLayout()
        self.search_result_layout.setSpacing(5)
        self.results_widget.setLayout(self.search_result_layout)

        # building a window:
        self.search_layout.addWidget(self.search_label, 0, 0)
        self.search_layout.addWidget(self.search_entry, 0, 1)
        self.v_box.addWidget(self.search_widget)
        self.results_labels_layout.addWidget(
            self.login_label, 0, 0, QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.results_labels_layout.addWidget(
            self.password_label, 0, 1, QtCore.Qt.AlignmentFlag.AlignHCenter
        )
        self.results_v_layout.addLayout(self.results_labels_layout)
        self.results_v_layout.addWidget(
            self.results_widget, 0, QtCore.Qt.AlignmentFlag.AlignTop
        )
        self.results_main_widget.setLayout(self.results_v_layout)
        self.results_area.setWidget(self.results_main_widget)
        self.buttons_layout.addWidget(
            self.search_button, 0, QtCore.Qt.AlignmentFlag.AlignRight
        )
        self.buttons_layout.addStretch()
        self.buttons_layout.addWidget(
            self.show_all_button, 0, QtCore.Qt.AlignmentFlag.AlignLeft
        )
        self.v_box.addWidget(self.results_area)
        self.v_box.addLayout(self.buttons_layout)
        self.v_box.addStretch()
        self.setLayout(self.v_box)

    def show_login_and_password(self, login, password, index):
        login_edit = QLineEdit(text=login)
        password_edit = QLineEdit(text=password)
        login_edit.setMinimumSize(QtCore.QSize(180, 30))
        password_edit.setMinimumSize(QtCore.QSize(280, 30))
        self.search_result_layout.addWidget(login_edit, index, 0)
        self.search_result_layout.addWidget(password_edit, index, 1)

    def clear_all_search_results(self):
        for i in range(self.search_result_layout.count()):
            self.search_result_layout.itemAt(i).widget().close()

    def notify_login_does_not_exists(self, login):
        self.search_entry.clear()
        self.search_entry.setStyleSheet('background-color: rgb(253, 187, 188)')
        self.search_entry.insert(f'{login}: Такого логина нет в базе данных!')

    def clear_search_entry(self):
        if re.search("Такого логина нет в базе данных!", self.search_entry.text()):
            self.search_entry.clear()
            self.search_entry.setStyleSheet('background-color: white;')
